//Import index.js from the the blocks,
//it isn't necessary to specify the file name as it's the default
import "./blocks/firstblock";
import "./blocks/secondblock";
import "./blocks/team-member";
