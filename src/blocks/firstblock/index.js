// var registerBlockType = wp.blocks.registerBlockType;
// var __ = wp.i18n.__; //import the translation function '__' from the internationalisation module.
// var el = wp.element.createElement;

const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n; //import the translation function '__' from the internationalisation module.
console.log("test");

registerBlockType("mytheme-blocks/firstblock", {
  title: __("First Block", "mytheme-blocks"),
  description: __("Our first block", "mytheme-blocks"),
  category: "layout",
  keywords: [__("photo", "mytheme-blocks"), __("image", "mytheme-blocks")],
  icon: {
    // background: '#0070de',
    // foreground: '#ffffff',
    src: "admin-network",
  },
  edit: function () {
    return <p>Edit . . .</p>;
  },
  save: function () {
    return <p>Save . . .</p>;
  },
});
