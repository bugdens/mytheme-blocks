import { Component } from "@wordpress/element";
import {
  RichText,
  BlockControls,
  AlignmentToolbar,
  InspectorControls,
  PanelColorSettings,
  withColors,
  ContrastChecker,
} from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { Toolbar, DropdownMenu } from "@wordpress/components";
import classnames from "classnames";
import { RangeControl, PanelBody } from "@wordpress/components";

class Edit extends Component {
  onChangeContent = (content) => {
    this.props.setAttributes({ content });
  };

  onChangeAlignment = (textAlignment) => {
    this.props.setAttributes({ textAlignment });
  };

  toggleShadow = () => {
    this.props.setAttributes({ shadow: !this.props.attributes.shadow });
    //alert(this.props.attributes.shadow);
  };

  onChangeShadowOpacity = (shadowOpacity) => {
    this.props.setAttributes({ shadowOpacity });
  };

  render() {
    //console.log(this.props);
    // console.log("this");
    // console.log(this);
    const {
      className,
      attributes,
      setBackgroundColor, //** extract the functions */
      setTextColor,
      backgroundColor,
      textColor,
    } = this.props;
    //Extract the content from the attributes
    const { content, textAlignment, shadow, shadowOpacity } = attributes;

    const classes = classnames(className, {
      "has-shadow": shadow,
      [`has-shadow-opacity-${shadowOpacity * 100}`]: shadowOpacity,
    });

    return (
      //Fragment start
      <>
        <InspectorControls>
          <PanelBody title={__("Settings", "mytheme-blocks")}>
            {shadow && (
              <RangeControl
                label={__("Shadow Opacity", "mytheme-blocks")}
                value={shadowOpacity}
                onChange={this.onChangeShadowOpacity}
                min={0.1}
                max={0.4}
                step={0.1}
              />
            )}
          </PanelBody>
          <PanelColorSettings
            title={__("PanelColorControl", "mytheme-blocks")}
            colorSettings={[
              {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __("Background Colour", "mytheme-blocks"),
              },
              {
                value: textColor.color,
                onChange: setTextColor,
                label: __("Text Colour", "mytheme-blocks"),
              },
            ]}
          >
            <ContrastChecker
              textColor={textColor.color}
              backgroundColor={backgroundColor.color}
            />
          </PanelColorSettings>
        </InspectorControls>
        <BlockControls
          controls={[
            {
              icon: "wordpress",
              title: __("shadow", "mytheme-blocks"),
              onClick: this.toggleShadow,
              isActive: shadow,
            },
          ]}
        >
          <AlignmentToolbar
            value={textAlignment}
            onChange={(textAlignment) => this.onChangeAlignment(textAlignment)}
          />

          {content && content.length > 0 && (
            <Toolbar>
              <DropdownMenu
                icon="editor-table"
                label={__("test", "mytheme-blocks")}
                controls={[
                  [
                    {
                      icon: "wordpress",
                      title: __("test", "mytheme-blocks"),
                      onClick: () => alert(true),
                      // Highlight icon if active
                      isActive: false,
                    },
                  ],
                  [
                    {
                      icon: "wordpress",
                      title: __("test", "mytheme-blocks"),
                      onClick: () => alert(true),
                      isActive: false,
                    },
                  ],
                ]}
              ></DropdownMenu>
            </Toolbar>
          )}
        </BlockControls>
        <RichText
          tagName="h4"
          className={classes}
          onChange={this.onChangeContent}
          //Add the content to the value
          value={content}
          formattingControls={["bold"]}
          style={{
            textAlign: textAlignment,
            backgroundColor: backgroundColor.color,
            color: textColor.color,
          }}
        />
        {/* Fragment end */}
      </>
    );
  }
}

export default withColors("backgroundColor", { textColor: "color" })(Edit);
