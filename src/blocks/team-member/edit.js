import { Component } from "@wordpress/element";
import {
  RichText,
  MediaPlaceholder,
  BlockControls,
  MediaUpload,
  MediaUploadCheck,
  InspectorControls,
} from "@wordpress/block-editor";
import { __ } from "@wordpress/i18n";
import { isBlobURL } from "@wordpress/blob";
import {
  Spinner,
  withNotices,
  Toolbar,
  IconButton,
  PanelBody,
  TextareaControl,
  SelectControl,
} from "@wordpress/components";
import { withSelect } from "@wordpress/data";

class TeamMemberEdit extends Component {
  componentDidMount() {
    const { attributes, setAttributes } = this.props;
    const { url, id } = attributes;

    if (url && isBlobURL(url) && !id) {
      setAttributes({
        url: "",
        alt: "",
      });
    }
  }

  onChangeTitle = (title) => {
    this.props.setAttributes({ title });
  };

  onChangeInfo = (info) => {
    this.props.setAttributes({ info });
  };

  onSelectImage = ({ id, url, alt }) => {
    this.props.setAttributes({
      id,
      url,
      alt,
    });
  };

  //If we select a url we won't have an id because it isn't from our library.
  //We won't have an alt text either.
  onSelectURL = (url) => {
    this.props.setAttributes({
      url,
      id: null,
      alt: "",
    });
  };

  onUploadError = (message) => {
    const { noticeOperations } = this.props;
    noticeOperations.createErrorNotice(message);
  };

  removeImage = () => {
    this.props.setAttributes({
      url: "",
      alt: "",
      id: null,
    });
  };

  updateAlt = (alt) => {
    this.props.setAttributes({
      alt,
    });
  };

  getImageSizes() {
    // wp.data.select('core/editor').getEditorSettings().imageSizes
    // wp.data.select('core').getMedia('276');

    const { image, imageSizes } = this.props;
    if (!image) return [];
    let options = []; //Create an array
    const sizes = image.media_details.sizes;
    for (const key in sizes) {
      //key is full, large medium etc.
      const size = sizes[key];

      //Check if size exists in the imageSizes array
      const imageSize = imageSizes.find((size) => size.slug === key);
      if (imageSize) {
        options.push({
          label: imageSize.name,
          value: size.source_url,
        });
      }
    }
    return options;
  }

  onImageSizeChange = (url) => {
    //alert("onImageSizeChange");
    this.props.setAttributes({
      url,
    });
  };

  render() {
    const { className, attributes, noticeUI } = this.props;
    const { title, info, url, alt, id } = attributes;
    // console.log("this.props");
    // console.log(this.props);
    // console.log("id = " + id);

    return (
      <>
        <InspectorControls>
          <PanelBody title={__("image Settings", "mytheme-blocks")}>
            {url && !isBlobURL(url) && (
              <TextareaControl
                label={__("Alt Text (Alternative Text)", "mytheme-blocks")}
                value={alt}
                onChange={this.updateAlt}
                help={__(
                  "Alternative text describes your image to people who can't see it. Add a short desciption with its key details.",
                  "mytheme-blocks"
                )}
              />
            )}
            {id && (
              <SelectControl
                label={__("Image Size", "mytheme-blocks")}
                options={this.getImageSizes()}
                onChange={this.onImageSizeChange}
                value={url}
              />
            )}
          </PanelBody>
        </InspectorControls>
        <BlockControls>
          {url && (
            <Toolbar>
              {id && (
                <MediaUploadCheck>
                  <MediaUpload
                    onSelect={this.onSelectImage}
                    allowedTypes={["image"]}
                    value={id}
                    render={({ open }) => {
                      return (
                        <IconButton
                          className="components-icon-button components-toolbar__control"
                          label={__("Edit Image", "mytheme-blocks")}
                          onClick={open}
                          icon="edit"
                        />
                      );
                    }}
                  />
                </MediaUploadCheck>
              )}
              <IconButton
                className="components-icon-button components-toolbar__control"
                label={__("Remove Image", "mytheme-blocks")}
                onClick={this.removeImage}
                icon="trash"
              />
            </Toolbar>
          )}
        </BlockControls>
        <div className={className}>
          {url ? (
            <>
              <img src={url} alt={alt} />
              {isBlobURL(url) && <Spinner />}
            </>
          ) : (
            <MediaPlaceholder
              icon="format-image"
              onSelect={this.onSelectImage}
              onSelectURL={this.onSelectURL}
              onError={this.onUploadError}
              //accept="image/*"
              allowTypes={["image"]}
              notices={noticeUI}
            />
          )}

          <RichText
            className={"wp-block-mytheme-team-member__title"}
            tagName="h4"
            onChange={this.onChangeTitle}
            value={title}
            placeholder={__("Member Name", "mytheme-blocks")}
            formattinControls={[]}
          />
          <RichText
            className={"wp-block-mytheme-team-member__info"}
            tagName="p"
            onChange={this.onChangeInfo}
            value={info}
            placeholder={__("Member Info", "mytheme-blocks")}
            formattinControls={[]}
          />
        </div>
      </>
    );
  }
}

export default withSelect((select, props) => {
  const id = props.attributes.id;
  return {
    image: id ? select("core").getMedia(id) : null,
    imageSizes: select("core/editor").getEditorSettings().imageSizes,
  };
})(withNotices(TeamMemberEdit));
