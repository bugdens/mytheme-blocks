import { registerBlockType } from "@wordpress/blocks";
import { __ } from "@wordpress/i18n";
import { InnerBlocks, InspectorControls } from "@wordpress/block-editor";
import { RangeControl, PanelBody } from "@wordpress/components";

const attributes = {
  columns: {
    type: "number",
    default: 3,
  },
};

registerBlockType("mytheme-blocks/team-members", {
  title: __("Team Members", "mytheme-blocks"),
  description: __("Block showing team members", "mytheme-blocks"),
  category: "mytheme-category",
  keywords: [
    __("Team", "mytheme-blocks"),
    __("member", "mytheme-blocks"),
    __("person", "mytheme-blocks"),
  ],
  supports: {
    html: false,
    align: ["wide", "full"],
  },
  icon: "grid-view",
  attributes,
  edit({ className, attributes, setAttributes }) {
    const { columns } = attributes;
    return (
      <div className={`${className} has-${columns}-columns`}>
        <InspectorControls>
          <PanelBody>
            <RangeControl
              label={__("Columns", "mytheme-blocks")}
              value={columns}
              onChange={(columns) => setAttributes({ columns })}
              min={1}
              max={6}
            />
          </PanelBody>
        </InspectorControls>

        <InnerBlocks
          allowedBlocks={["mytheme-blocks/team-member"]}
          template={[
            ["mytheme-blocks/team-member"],
            ["mytheme-blocks/team-member"],
            ["mytheme-blocks/team-member"],
          ]}
          //   templateLock={"insert"}
        />
      </div>
    );
  },
  save({ attributes }) {
    const { columns } = attributes;
    return (
      <div className={`has-${columns}-columns`}>
        <InnerBlocks.Content />
      </div>
    );
  },
});
