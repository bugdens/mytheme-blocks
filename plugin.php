<?php
/*
Plugin Name: mytheme-blocks
Plugin URI: 
Description: 
Version: 1.0.0
Author: Stephen Bugden
Author URI: 
*/

//Protect against accessing this file directly.
if (!defined('ABSPATH')) {
    exit;
}

function mytheme_blocks_categories ($categories, $post) {
return array_merge(
    $categories,
    array(
        array(
            'slug' => 'mytheme-category',
            'title' => __('My Theme Category', 'mytheme-blocks'),
            'icon' => 'wordpress'
        )
    )
    );
}

add_filter('block_categories', 'mytheme_blocks_categories', 10, 2);

//Use an $options parameter initialised to an empty array to provide additional options if required.
function mytheme_blocks_register_block_type($block, $options = array())
{
    //Specify the namespace/name of our block which must match the javascript registration
    register_block_type(
        'mytheme-blocks/' . $block,
        //Merge the options array into our existing array
        array_merge(
            array(
                //Remember that we're using the script name defined in wp_register_scrpt() below.
                'editor_script' => 'mytheme-blocks-editor-script',
                'editor_style' => 'mytheme-blocks-editor-style',
                'script' => 'mytheme-blocks-script',
                'style' => 'mytheme-blocks-style'
            ),
            $options
        )
    );
}

function mytheme_blocks_register()
{
    //Register our index.js file, the first parameter is a unique name which 
    //we can use to refer to our script.
    wp_register_script(
        //Unique name for our block
        'mytheme-blocks-editor-script',
        //The file itself
        plugins_url('dist/editor.js', __FILE__),
        //specify dependencies which need to be registered before our block
        array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-components', 'wp-block-editor', 'wp-blob', 'wp-data') 
    );

    wp_register_script(
        //Unique name for our block
        'mytheme-blocks-script',
        //The file itself
        plugins_url('dist/script.js', __FILE__),
        //specify dependencies which need to be registered before our block
        array('jquery') 
    );

    wp_register_style(
        'mytheme-blocks-editor-style',
        plugins_url('dist/editor.css', __FILE__),
        array('wp-edit-blocks')
    );

    wp_register_style(
        'mytheme-blocks-style',
        plugins_url('dist/style.css', __FILE__)
    );

    mytheme_blocks_register_block_type('firstblock');
    mytheme_blocks_register_block_type('secondblock');
    mytheme_blocks_register_block_type('team-member');
    mytheme_blocks_register_block_type('team-members');
}

//Use the init action to register our block
add_action('init','mytheme_blocks_register');